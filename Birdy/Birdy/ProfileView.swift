//
//  ProfileView.swift
//  Birdy
//
//  Created by student on 11.01.2024..
//

import SwiftUI

struct ProfileView: View {
    
    @EnvironmentObject var tweetData: TweetData
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        VStack{
            HStack{
                Image(userData.imageName)
                    .resizable()
                    .frame(width: 60, height: 60)
                    .clipShape(Circle())
                Text(userData.username)
                    .font(.subheadline)
                Spacer()
            }
            .padding()
            
            Text("My tweets")
            List(Binding.constant(tweetData.getTweets(inds:userData.myTweetsIds))){ tweet in
                TweetRow(tweet: tweet)
            }
            .listStyle(.plain)
            .padding()
            
            Text("Liked tweets")
            List(Binding.constant(tweetData.getTweets(inds: userData.likedTweetsIds))){ tweet in
                TweetRow(tweet: tweet)
            }
            .listStyle(.plain)
            .padding()

            Spacer()
        }
    }
}

#Preview {
    ProfileView()
        .environmentObject(TweetData())
        .environmentObject(UserData())
}
