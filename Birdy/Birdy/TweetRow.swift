//
//  TweetRow.swift
//  Birdy
//
//  Created by student on 19.12.2023..
//

import SwiftUI

struct TweetRow: View {
    
    @Binding var tweet: Tweet
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        HStack{
            Image("Booby")
                .resizable()
                .frame(width: 55, height: 55)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            VStack(alignment: .leading){
                Text(tweet.username)
                    .foregroundColor(.gray)
                    .font(.subheadline)
                    .padding(.bottom, 1)
                Text(tweet.content)
                Text(tweet.date, style: .relative)
                    .font(.caption)
                    .foregroundColor(.gray)
            }
            .padding(.leading)
            Spacer()
            Button(action: {
                tweet.isFavorite.toggle()
                if tweet.isFavorite {
                    userData.likedTweetsIds.append(tweet.id)
                }
                else {
                    if let ind = userData.likedTweetsIds.firstIndex(of: tweet.id){
                        userData.likedTweetsIds.remove(at: ind)
                    }
                }
            }) {
                if tweet.isFavorite
                {Image(systemName: "heart.fill")
                        .foregroundStyle(.pink)
                } else {Image(systemName: "heart")
                        .foregroundStyle(.pink)
                }
            }
        }
    }
}

#Preview {
    TweetRow(tweet: Binding.constant(
        Tweet(username: "luka", content: "i ja", isFavorite: true))
    )
        .environmentObject(UserData())
}
