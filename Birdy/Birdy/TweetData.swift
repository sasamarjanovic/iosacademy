//
//  TweetData.swift
//  Birdy
//
//  Created by student on 11.01.2024..
//

import Foundation


class TweetData: ObservableObject {
    @Published var tweets: [Tweet] = [
        Tweet(username: "heda", content: "prvi", isFavorite: false),
        Tweet(username: "heda", content: "drugi", isFavorite: false),
        Tweet(username: "heda", content: "treci", isFavorite: false),
        Tweet(username: "heda", content: "cet", isFavorite: false)
    ]
    
    func getTweets(inds: [String]) -> [Tweet]{
        return tweets.filter { tweet in
            return inds.contains(tweet.id)
        }
    }
}
