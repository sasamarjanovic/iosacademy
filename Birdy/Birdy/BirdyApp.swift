//
//  BirdyApp.swift
//  Birdy
//
//  Created by student on 13.12.2023..
//

import SwiftUI

@main
struct BirdyApp: App {
    
    @StateObject var tweetData = TweetData()
    @StateObject var userData = UserData()
    
    var body: some Scene {
        WindowGroup {
            TabView {
                ContentView()
                    .tabItem {
                        Label("Tweets", systemImage: "list.bullet.circle")
                    }
                SearchView()
                    .tabItem {
                        Label("Search", systemImage: "magnifyingglass.circle")
                    }
                ProfileView()
                    .tabItem {
                        Label("Profile", systemImage: "person.circle")
                    }
            }
            .environmentObject(tweetData)
            .environmentObject(userData)
        }
    }
}
