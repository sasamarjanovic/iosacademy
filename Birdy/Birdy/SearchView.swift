//
//  SearchView.swift
//  Birdy
//
//  Created by student on 11.01.2024..
//

import SwiftUI

struct SearchView: View {
    
    @EnvironmentObject var tweetData: TweetData
    
    @State var query = ""
    
    var foundedTweets: [Tweet] {
        if query.isEmpty {
            return tweetData.tweets
        }
        else {
            return tweetData.tweets.filter {tweet in
                return tweet.username.contains(query) || tweet.content.contains(query)
            }
        }
    }
    
    var body: some View {
        VStack {
            TextField("Search", text: $query)
                .autocapitalization(/*@START_MENU_TOKEN@*/.none/*@END_MENU_TOKEN@*/)
                .padding()
            
            List(Binding.constant(foundedTweets)){ tweet in
                TweetRow(tweet: tweet)
            }
            .listStyle(.plain)
            
            Spacer()
        }
    }
}

#Preview {
    SearchView()
        .environmentObject(TweetData())
}
